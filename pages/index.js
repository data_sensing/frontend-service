import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import IconLogo from '../public/asset/Icon_Main.png'
import styles from '@/styles/Home.module.css'
import { Box, Grid } from '@material-ui/core'
const inter = Inter({ subsets: ['latin'] })

const Home = () => {
  return (
    <>
      <Head>
        <title>Data Sensing Systesm</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
        <link rel="icon" href='/asset/Icon_Main.png'/>
      </Head>

      <Box sx={{ flexGrow: 1 }} >
        <Grid container direction="row" justifyContent='flex-start' alignItems="center" marginBlockEnd= '300'>
          <Grid item xs={5} style={{textAlign: 'center'}} >
            <Box display='inline-block' height="500">
            <img src={"/asset/AIF_Purple_noncrop.png"} style={{ height: 202 }}/>
              <Box style={{ fontSize: '32px', fontWeight: 'bolder', color: '#5D40D2'}}>
                Data Sensing System
              </Box>
            </Box>
          </Grid>
          <Grid item xs={7}>
          

            <Box>
            <img src={"/asset/bg_login.jpg"} style={{ width: '100%', height: '100vh' }}/>
            </Box>
          </Grid>
        </Grid>
      </Box>
   
    </>
  )
}
export default Home
